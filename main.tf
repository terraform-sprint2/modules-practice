module "vpc" {
    source = ".//modules/VPC"
    name = var.name
    region = var.region
    auto_create_subnetworks = var.auto_create_subnetworks
}
module "vpc-subnet" {
    source = ".//modules/subnet"
    name = var.name
    region = var.region
    ip_cidr_range-private = var.ip_cidr_range-private
    googleaccess-private = var.googleaccess-private
    ip_cidr_range-public = var.ip_cidr_range-public
    googleaccess-public = var.googleaccess-public
    vpc = module.vpc.vpc_id
}
module "vpc-firewall" {
    source = ".//modules/firewall"
    name = var.name
    ports = var.ports
    range = var.range
    vpc = module.vpc.vpc_id
}
module "vpc-nat" {
    source = ".//modules/nat"
    name = var.name
    region = var.region
    vpc = module.vpc.vpc_id
    natsubnet = module.vpc-subnet.subnet_private
}
module "vpc-vm" {
    source = ".//modules/vm"
    name = var.name
    zone = var.zone
    vpc = module.vpc.vpc_id
    machinetype = var.machinetype
    image = var.image
    vmprivatesubnet = module.vpc-subnet.subnet_private
    vmpublicsubnet = module.vpc-subnet.subnet_public
}