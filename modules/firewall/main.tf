resource "google_compute_firewall" "vpc-firewall" {
    name = "${var.name}-firewall"
    network = var.vpc
    allow {
      protocol = "icmp"
    }
    allow {
      protocol = "tcp"
      ports = var.ports
    }
    source_ranges = var.range
}