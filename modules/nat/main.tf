resource "google_compute_router" "router" {
    name = "${var.name}-route"
    network = var.vpc
    region = "${var.region}"
    bgp {
    asn = 64514
  }
}

//Creating NAT gateway
resource "google_compute_router_nat" "nat" {
    name = "${var.name}-nat"
    router = google_compute_router.router.name
    region = google_compute_router.router.region
    nat_ip_allocate_option = "AUTO_ONLY"
    source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

    subnetwork {
      name = var.natsubnet
      source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
    }
}