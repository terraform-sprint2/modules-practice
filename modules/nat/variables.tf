variable "name" {
    type = string
}   
variable "vpc" {}
variable "region" {
    type = string
}
variable "natsubnet" {}