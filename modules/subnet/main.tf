resource "google_compute_subnetwork" "private" {
    name = "${var.name}-private"
    ip_cidr_range = var.ip_cidr_range-private
    region = var.region
    private_ip_google_access = var.googleaccess-private
    network = var.vpc
}

resource "google_compute_subnetwork" "public" {
    name = "${var.name}-public"
    ip_cidr_range = var.ip_cidr_range-public
    region = var.region
    private_ip_google_access = var.googleaccess-public
    network = var.vpc
}