output "subnet_private" {
    value = google_compute_subnetwork.private.id
}
output "subnet_public" {
    value = google_compute_subnetwork.public.name
}