variable "region" {
    type = string
    description = "To store region"
}
variable "name" {
    type = string
    description = "To store the name of variable"
}
variable "ip_cidr_range-private" {
  
}
variable "googleaccess-private" {
  
}
variable "googleaccess-public" {
  
}
variable "ip_cidr_range-public" {
  
}
variable "vpc" {
  
}