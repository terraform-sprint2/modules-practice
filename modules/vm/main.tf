resource "google_compute_instance" "public-vm" {
    name = "${var.name}-public-vm"
    machine_type = "${var.machinetype}"
    zone = "${var.zone}"
    boot_disk {
      initialize_params {
          image = "${var.image}"
      }
    }
    network_interface {
      network = var.vpc
      subnetwork = var.vmpublicsubnet
      access_config {
        
      }

    }
    tags = ["http-server","https-server"]
    metadata_startup_script = "sudo apt update && sudo apt -y install apache2"
}

resource "google_compute_instance" "private-vm" {
    name = "${var.name}-privat-vm"
    machine_type = "${var.machinetype}"
    zone = "${var.zone}"
    boot_disk {
      initialize_params {
          image = "${var.image}"
      }
    }
    network_interface {
      network = var.vpc
      subnetwork = var.vmprivatesubnet
    }
    tags = ["http-server","https-server"]
    metadata_startup_script = "sudo apt update && sudo apt -y install mysql-server && sudo systemctl start mysql.service"
}