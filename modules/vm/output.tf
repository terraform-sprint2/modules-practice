output "private-vm" {
    value = google_compute_instance.private-vm.id
}
output "public-vm" {
    value = google_compute_instance.public-vm.id
}