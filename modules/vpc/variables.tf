variable "region" {
    type = string
    description = "To store region"
}
variable "name" {
    type = string
    description = "To store the name of variable"
}
variable "auto_create_subnetworks" {
    type = bool
    description = "To store yes/no for creating subnets"
}

