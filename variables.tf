variable "project" {
    type = string
    description = "To store project id"  
}
variable "region" {
    type = string
    description = "To store region"
}
variable "name" {
    type = string
    description = "To store the name of variable"
}
variable "auto_create_subnetworks" {
    type = bool
    description = "To store yes/no for creating subnets"
}
variable "ip_cidr_range-private" {
    type = string
}
variable "googleaccess-private" {
    type = bool
}
variable "googleaccess-public" {
    type = bool
}
variable "ip_cidr_range-public" {
        type = string
}
variable "ports" {
    type = list(string)
}
variable "range" {
    type = list(string)
}
variable "zone" {
    type = string
}
variable "machinetype" {
  
}
variable "image" {
  
}